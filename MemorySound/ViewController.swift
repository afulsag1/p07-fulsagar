//
//  ViewController.swift
//  MemorySound
//
//  Created by Abhijit Fulsagar on 4/29/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

import UIKit
import AVFoundation
class ViewController: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var scorelabel: UILabel!
    @IBOutlet weak var highscorelabel: UILabel!
    @IBOutlet weak var playbutton: UIButton!
    @IBOutlet var soundbutton: [UIButton]!
    @IBOutlet weak var label: UILabel!
    var player1:AVAudioPlayer!
    var player2:AVAudioPlayer!
    var player3:AVAudioPlayer!
    var player4:AVAudioPlayer!
    var playlist=[Int]()
    var item=0
    var taps=0
    var userinput=false
    var level=1
    var score=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupaudiofiles()
    }
    
    func  checkingpattern (a:Int)
    {
        if a==playlist[taps]
        {
            if taps==(playlist.count-1)
            {
                let delayInSeconds = 2.0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    self.nextround()
                }
                return
            }
            taps=taps+1
        }
        else
        {
            //reset game
            reset()
        }
    }
    
    func nextround()
    {
        level=level+1;
        score=score+5
        label.text="Level \(level)"
        scorelabel.text="\(score)"
        userinput=false
        item=0
        taps=0
        disablebutton()

        let number=Int(arc4random_uniform(4)+1)
        playlist.append(number)
        playnextitem()
            }
    
    func reset()
    {
        level=1
        label.text="Game Over"
        scorelabel.text="0"
        score=0
        taps=0
        item=0
        userinput=false
        playlist=[]
        self.playbutton.isHidden=false
        disablebutton()

    }
    
    @IBAction func playbuttonpressed(_ sender: Any) {
        label.text="Level 1"
        scorelabel.text="0"
        score=0

        disablebutton()
        let number=Int( arc4random_uniform(4)+1)
        playlist.append(number)
        self.playbutton.isHidden=true
        playnextitem()
    }
    
    func setupaudiofiles()
    {
        let sound1 = NSURL(fileURLWithPath: Bundle.main.path(forResource: "1", ofType: "wav")!)
        let sound2 = NSURL(fileURLWithPath: Bundle.main.path(forResource: "2", ofType: "wav")!)
        let sound3 = NSURL(fileURLWithPath: Bundle.main.path(forResource: "3", ofType: "wav")!)
        let sound4 = NSURL(fileURLWithPath: Bundle.main.path(forResource: "4", ofType: "wav")!)
        do
        {
            player1 = try!AVAudioPlayer(contentsOf:sound1 as URL)
            player2 = try!AVAudioPlayer(contentsOf:sound2 as URL)
            player3 = try!AVAudioPlayer(contentsOf:sound3 as URL)
            player4 = try!AVAudioPlayer(contentsOf:sound4 as URL)
            
        }
        catch
        {
            print(error)
        }
        player1.delegate=self
        player2.delegate=self
        player3.delegate=self
        player4.delegate=self
        player1.numberOfLoops=0
        player2.numberOfLoops=0
        player3.numberOfLoops=0
        player4.numberOfLoops=0
        
        /*var audioPlayer = AVAudioPlayer()
         audioPlayer = try!AVAudioPlayer(contentsOf:coinSound as URL)
         audioPlayer.prepareToPlay()
         audioPlayer.play()*/
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if item<=playlist.count-1 {
           playnextitem()
        } else {
            userinput=true
            resetbuttonhighlight()
            enablebutton()
        }
    }
    @IBAction func soundbuttonpressed(_ sender: AnyObject) {
        
        if userinput
        {
            let button=sender as!UIButton
            switch button.tag {
            case 1:
                player1.play()
                checkingpattern(a: 1)
                break
            case 2:
                player2.play()
                checkingpattern(a: 2)
                break
            case 3:
                player3.play()
                checkingpattern(a: 3)
                break
            case 4:
                player4.play()
                	checkingpattern(a: 4)
                break
            default:
                break
            }
        }
        
    }
    
    func playnextitem(){
        let randomitem=playlist[item]
        switch randomitem {
        case 1:
            buttonhighlight(index: 1)
            player1.play()
            break
        case 2:buttonhighlight(index: 2)
        player2.play()
            break
        case 3:buttonhighlight(index: 3)
        player3.play()
            break
        case 4:buttonhighlight(index: 4)
        player4.play()
            break
        default:break
        }
        item=item+1
    }
    
    func buttonhighlight(index:Int)
    {
        switch index {
        case 1:
            resetbuttonhighlight()
            soundbutton[index-1].setImage(UIImage(named: "redpressed")?.withRenderingMode(.alwaysOriginal), for: .normal)
            break
        case 2:
            resetbuttonhighlight()
            soundbutton[index-1].setImage(UIImage(named: "yellowpressed")?.withRenderingMode(.alwaysOriginal), for: .normal)
            break
        case 3:
            resetbuttonhighlight()
            soundbutton[index-1].setImage(UIImage(named: "bluepressed")?.withRenderingMode(.alwaysOriginal), for: .normal)
            break
        case 4:
            resetbuttonhighlight()
            soundbutton[index-1].setImage(UIImage(named: "greenpressed")?.withRenderingMode(.alwaysOriginal), for: .normal)
            break
        default:
            break
        }
        
    }
    
    func resetbuttonhighlight()
    {
        soundbutton[0].setImage(UIImage(named: "red")?.withRenderingMode(.alwaysOriginal), for: .normal)
        soundbutton[1].setImage(UIImage(named: "yellow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        soundbutton[2].setImage(UIImage(named: "blue")?.withRenderingMode(.alwaysOriginal), for: .normal)
        soundbutton[3].setImage(UIImage(named: "green")?.withRenderingMode(.alwaysOriginal), for: .normal)
        
    }
    
   func disablebutton()
   {
        for currentbutton in soundbutton
        {
            currentbutton.isUserInteractionEnabled=false
        }
   }
   
    func enablebutton()
    {
        for currentbutton in soundbutton
        {
            currentbutton.isUserInteractionEnabled=true
        }
    }
    
           override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

